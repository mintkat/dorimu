extends Spatial


onready var voxel_tools = $VoxelTools
onready var menu_button = $Toolbar/MenuButton
onready var editor_view = $EditorView
onready var script_editor = $ScriptEditor

# Called when the node enters the scene tree for the first time.
func _ready():
	voxel_tools.hide()
	$MapObjectsPanel/Scene.connect("add_empty", self, "_add_empty_map_object")
	menu_button.get_popup().add_item("Open File")
	menu_button.get_popup().add_item("Save as File")
	menu_button.get_popup().add_item("Quit to Menu")
	menu_button.get_popup().add_item("Quit Dorimu")
	
	menu_button.get_popup().connect("id_pressed", self, "_on_item_pressed")
	
	script_editor.hide()

func _on_item_pressed(id):
	var item_name = menu_button.get_popup().get_item_text(id)
	if item_name == 'Open File':
		pass
	elif item_name == 'Save as File':
		pass
	elif item_name == 'Quit to Menu':
		get_tree().change_scene("res://source/main.tscn")
	elif item_name == 'Quit Dorimu':
		get_tree().quit()
	print(item_name + ' pressed')

func show_voxel_tools():
	voxel_tools.show()
	editor_view.block_edit_mode = true
	script_editor.hide()

func _on_ScriptButton_pressed():
	script_editor.show()
	editor_view.block_edit_mode = false
	voxel_tools.hide()


func _object_mode():
	editor_view.block_edit_mode = false
	voxel_tools.hide()


func _add_empty_map_object():
	var map_object = preload("res://source/runtime/map_objects/map_object.tscn")
	var instance = map_object.instance()
	add_child(instance)

func _add_ball():
	var ball = preload("res://source/runtime/map_objects/ball.tscn")
	var instance = ball.instance()
	add_child(instance)

