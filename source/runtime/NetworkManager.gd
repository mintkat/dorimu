extends Node

func _ready():
	Client.connect("lobby_joined", self, "_room_joined")
	Client.connect("connected", self, "_connected")
	Client.connect("disconnected", self, "_disconnected")
	Client.rtc_mp.connect("peer_connected", self, "_mp_peer_connected")
	Client.rtc_mp.connect("peer_disconnected", self, "_mp_peer_disconnected")
	Client.rtc_mp.connect("server_disconnected", self, "_mp_server_disconnect")
	Client.rtc_mp.connect("connection_succeeded", self, "_mp_connected")


func _process(delta):
	Client.rtc_mp.poll()
	while Client.rtc_mp.get_available_packet_count() > 0:
		print(Client.rtc_mp.get_packet().get_string_from_utf8())


func _connected(id):
	print("")


func _listen():
	Server.listen(9080)


func _connect():
	pass # Replace with function body.
