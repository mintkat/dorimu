extends Control

onready var connect_dialog = $ConnectDialog
onready var rooms_dialog = $RoomsDialog

func _init():
	Client.rtc_mp.connect("peer_connected", self, "_on_connected")

func _ready():
	connect_dialog.show()


func _on_connect():
	connect_dialog.hide()
	rooms_dialog.show()
