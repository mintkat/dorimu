extends Node

const DEFAULT_PORT = 28960
const MAX_CLIENTS = 6

var server = null
var client = null

var ip_address = "127.0.0.1"

var network_protocol = ""

func _ready():
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	get_tree().connect("connection_failed", self, "_connection_failed")
	get_tree().connect("network_peer_connected", self, "_player_connected")

func create_server():
	print("Creating Server")
	if network_protocol == "ENet":
		server = NetworkedMultiplayerENet.new()
		server.create_server(DEFAULT_PORT, MAX_CLIENTS)
		get_tree().set_network_peer(server)
	elif network_protocol == "WebSockets":
		var server = WebSocketServer.new()
		server.listen(DEFAULT_PORT, PoolStringArray(), true)
		get_tree().set_network_peer(server)

func join_server():
	print("Joining Server")
	
	if network_protocol == "ENet":
		client = NetworkedMultiplayerENet.new()
		client.create_client(ip_address, DEFAULT_PORT)
		get_tree().set_network_peer(client)
	elif network_protocol == "WebSockets":
		var client = WebSocketClient.new()
		var url = "ws://" + ip_address + ":" + str(DEFAULT_PORT)
		var error = client.connect_to_url(url, PoolStringArray(), true)
		get_tree().set_network_peer(server)

func _connected_to_server():
	print("Successfully connected to the server")

func _server_disconnected():
	print("Disconnected from the server")
	
	reset_network_connection()

func _connection_failed():
	print("Connection to server failed")
	
	reset_network_connection()

func _player_connected(id):
	print("Player connected " + str(id))

func reset_network_connection():
	if get_tree().has_network_peer():
		get_tree().network_peer = null
