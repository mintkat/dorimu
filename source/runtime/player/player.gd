extends KinematicBody

export var jump_strength := 10.0

var gravity = -40
var speed = 4
var jump_speed = 10
var mouse_sensitivity = 0.08

var velocity = Vector3()

var snap_vector = Vector3.DOWN
var motion = Vector3.ZERO

onready var head = $Head
onready var model = $Skin

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	model.visible = false

func get_input():
	var input_dir = Vector3()
	
	if Input.is_action_pressed("move_forward"):
		input_dir += -global_transform.basis.z
	if Input.is_action_pressed("move_backward"):
		input_dir += global_transform.basis.z
	if Input.is_action_pressed("move_left"):
		input_dir += -global_transform.basis.x
	if Input.is_action_pressed("move_right"):
		input_dir += global_transform.basis.x
	
	input_dir = input_dir.normalized()
	return input_dir

func _input(event):
	if event is InputEventMouseMotion:
		rotate_y(deg2rad(-event.relative.x * mouse_sensitivity))
		head.rotate_x(deg2rad(-event.relative.y * mouse_sensitivity))
		head.rotation.x = clamp(head.rotation.x, deg2rad(-90), deg2rad(90))

func _physics_process(delta):
	velocity.y += gravity * delta
	
	var desired_velocity = get_input() * speed
	velocity.x = desired_velocity.x
	velocity.z = desired_velocity.z
	
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y += jump_speed
	
	velocity = move_and_slide(velocity, Vector3.UP, true)
	
	#var move_direction := Vector3.ZERO
	#move_direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	#move_direction.z = Input.get_action_strength("move_back") - Input.get_action_strength("move_forward")
	#move_direction = move_direction.rotated(Vector3.UP, head.rotation.y).normalized()
	#
	#
	#velocity.x = move_direction.x * speed
	#velocity.z = move_direction.z * speed
	#velocity.y -= gravity * delta
	
	#var just_landed = is_on_floor() and snap_vector == Vector3.ZERO
	#var is_jumping := is_on_floor() and Input.is_action_just_pressed("jump")
	#if is_jumping:
	#	velocity.y = jump_strength
	#	snap_vector = Vector3.ZERO
	#elif just_landed:
	#	snap_vector = Vector3.DOWN
	
	#velocity = move_and_slide_with_snap(velocity, snap_vector, Vector3.UP, true)

